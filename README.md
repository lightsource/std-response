# Std Response

## What is it
Standardization for a function / method response with multiple arguments

## Installation
```
composer require lightsource/std-response
```

## Example of usage

```
use LightSource\StdResponse\StdResponse;

require_once __DIR__ . '/vendor/autoload.php';

function calc($a, $b)
{
    $value     = $a + $b;
    $isSuccess = $value > $a;

    return new StdResponse($isSuccess, ['value' => 10,]);
}

$response = calc(10, 20);

if ($response->isSuccess()) {
    $value = $response->getArgs()['value'];
    // todo
} else {
    $errorMessages = $response->getErrorMessages();
    // todo
}
```
