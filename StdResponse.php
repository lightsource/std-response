<?php

namespace LightSource\StdResponse;

class StdResponse
{
    private bool $isSuccess;
    private array $args;
    private array $errorMessages;

    public function __construct(bool $isSuccess = false, array $args = [], array $errorMessages = [])
    {
        $this->isSuccess     = $isSuccess;
        $this->args          = $args;
        $this->errorMessages = $errorMessages;
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function getArgs(): array
    {
        return $this->args;
    }

    public function getErrorMessages(): array
    {
        return $this->errorMessages;
    }

    public function setIsSuccess(bool $isSuccess): void
    {
        $this->isSuccess = $isSuccess;
    }

    public function setArgs(array $args): void
    {
        $this->args = $args;
    }

    public function setErrorMessages(array $errorMessages): void
    {
        $this->errorMessages = $errorMessages;
    }
}
